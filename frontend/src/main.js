var auto_reader;
var data_big = [];
var data_auto;

var current_values = {};


var url_data_auto = './src/frontdata.json';

$.getJSON(url_data_auto, function (json) {
   data_auto = json;
});

var counter = 0;

function mode_manu() {
   setValues(data_auto[counter].arvot.lampo, data_auto[counter].arvot.kosteus)
   counter += 1;
}


function setValues(tamp, fum) {
   if (tamp <= 20) {
      document.getElementById('data-temp').innerText = "L0";
      document.getElementById('img-temp').setAttribute('src', "src/img/fan_off.png");
   } else if (tamp > 20 && tamp < 25) {
      document.getElementById('data-temp').innerText = "L1";
      document.getElementById('img-temp').setAttribute('src', "src/img/fan_1.png");
   } else if (tamp => 20) {
      document.getElementById('data-temp').innerText = "L2";
      document.getElementById('img-temp').setAttribute('src', "src/img/fan_2.png");
   }

   if (fum <= 30) {
      document.getElementById('data-humidity').innerText = "K2";
      document.getElementById('img-humidity').setAttribute('src', "src/img/hum_2.png");
   } else if (fum > 30 && fum < 36) {
      document.getElementById('data-humidity').innerText = "L1";
      document.getElementById('img-humidity').setAttribute('src', "src/img/hum_1.png");
   } else if (fum > 36) {
      document.getElementById('data-humidity').innerText = "L2";
      document.getElementById('img-humidity').setAttribute('src', "src/img/hum_off.png");
   }
   current_values = {
      tuuletin:document.getElementById('data-temp').innerText,
      kosteus:document.getElementById('data-humidity').innerText
   }
}

function writeToFile(file,data) {
   
}

$(document).ready(function () {
   $('#button-state-off').click(function () {
      document.getElementById('data-state').innerText = "OFF";
      clearInterval(auto_reader);
      writeToFile('./muutalaite-OFF.json',{
         tuuletin: 'L0',
         kosteus: 'K0'
      });
      setValues(0,50);

   });

   $('#button-state-auto').click(function () {
      document.getElementById('data-state').innerText = "AUTO";
      auto_reader = setInterval(mode_manu, 10000);
   });

   $('#button-state-manu').click(function () {
      document.getElementById('data-state').innerText = "MAN";

      function ask() {
         let good1 = false;
         let good2 = false;
         let fan = prompt('Tuulettimen voimakkuus');
         let hum = prompt('Kosteuden voimakkuus');

         if (fan == "L0" || fan == "L1" || fan == "L2") {
            good1 = true;
         } else
            good1 = false;
         if (hum == "K0" || hum == "K1" || hum == "K2") {
            good2 = true;
         } else
            good2 = false;

         if (good1 == true && good2 == true) {
            return {
               fan: fan,
               hum: hum
            };
         } else
            ask();
      }

      var values = ask()

      writeToFile('./muutalaite-MAN.json',{
         tuuletin: values.fan,
         kosteus: values.hum
      });
   });
});