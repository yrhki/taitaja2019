var jsonURL = 'https://public.bc.fi/katta/Taitaja2019/data.json'

$.getJSON(jsonURL, function (data) {
    var jsonData = data.reverse()

    var chartTemperatureDataMin = [];
    var chartTemperatureLabels = [];

    var chartHumidityDataMax = [0, 0];
    var chartHumidityLabels = [0, 0];


    jsonData.forEach(dataRow => {

        chartTemperatureDataMin.push(dataRow.min.temperature)
        chartTemperatureLabels.push(dataRow.group.hour + ':00')

        if (dataRow.group.hour === 18) {
            chartHumidityDataMax.splice(chartHumidityDataMax.length - 1, 0, dataRow.max.humidity)
            chartHumidityLabels.splice(chartHumidityLabels.length - 1, 0, dataRow.group.hour + ':00')
        }
    });



    var ctx_temperature = document.getElementById('temperature').getContext('2d');
    var chartTemperature = new Chart(ctx_temperature, {
        type: 'bar',
        responsive: true,
        data: {
            labels: chartTemperatureLabels,
            datasets: [{
                label: 'Lämpötila',
                data: chartTemperatureDataMin,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 10,
                        max: 30
                    }
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });
    var ctx_humidity = document.getElementById('humidity').getContext('2d');
    var chartHumidity = new Chart(ctx_humidity, {
        type: 'line',
        data: {
            labels: chartHumidityLabels,
            datasets: [{
                label: 'Kosteus',
                data: chartHumidityDataMax,
                borderWidth: 1,
                fill: false
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 10,
                        max: 50
                    }
                }],
                xAxes: [{
                    display: false
                }]
            }
        }
    });


});


function isMobile() {
    return window.getComputedStyle(document.getElementById('button-colorpicker')).display != "none" ? true : false;
}


$(window).on('load', function (e) {
    var color_selector = document.getElementById('dropdown-colors');
    for (let index = 0; index < color_selector.children.length; index++) {
        let elem = color_selector.children[index].firstElementChild.firstElementChild;
        elem.style.backgroundColor = elem.innerText;
    }

    var canvas = document.getElementById("canvas-house");
    var canvas_shapes = [
        {
            id:"floor",
            img_src:"src/img/floor.jpeg"
        }
    ];
    var ctx = canvas.getContext('2d');

    var color_selected = {
        floor: "",
        wall_bottom: "",
        wall_right: ""
    }
    // Fill shape from nodes
    function shapeDraw(nodes = [], fill, id, alpha = 0) {
        canvas_shapes.push({nodes:nodes,id:id});
        shapeDefine(nodes);
        ctx.globalAlpha = alpha;
        ctx.fillStyle = fill;
        ctx.fill();
    }

    // Make path from nodes
    function shapeDefine(nodes = []) {
        copy = nodes.slice(0);
        if (copy.length < 2)
            return;
        ctx.beginPath();
        ctx.moveTo(copy[0][0], copy[0][1]);
        copy.shift();
        copy.forEach(node => {
            ctx.lineTo(node[0], node[1]);
        });
    }

    function drawHouse() {
        canvas_shapes = [];
        

        let background_floor = new Image();
        background_floor.src = 'src/img/floor.jpeg';
        let background_bottom = new Image();
        background_bottom.src = 'src/img/wall1.jpeg';
        let background_wall_right = new Image();
        background_wall_right.src = 'src/img/wall2.jpg';

        // Floor
        background_floor.onload = function () {
            shapeDraw([[0, 0],[0, 240],[420, 240],[420, 0]], ctx.createPattern(this, "repeat"), 'floor', 1);
            ctx.globalAlpha = 0.5;
            ctx.fillStyle = color_selected.floor;
            ctx.fill();
        };

        // Wall down
        background_bottom.onload = function () {
            shapeDraw([[0, 240],[420, 240],[550, 370],[0, 370]], ctx.createPattern(this, "repeat"), 'wall_bottom', 1);
            ctx.globalAlpha = 0.5;
            ctx.fillStyle = color_selected.wall_bottom;
            ctx.fill();
        };

        // Wall right
        background_wall_right.onload = function () {
            shapeDraw([[420, 0],[420, 240],[550, 370],[550, 0]], ctx.createPattern(this, "repeat"), 'wall_right', 1);
            ctx.globalAlpha = 0.5;
            ctx.fillStyle = color_selected.wall_right;
            ctx.fill();
        };
    }
    drawHouse();



    // Color selected
    $('.color-item').on('click', function (e) {
        document.getElementById('color').style.backgroundColor = e.target.firstElementChild.innerText;
    });


    // Remove colors
    $('#color-remove').on('click', function () {
        var color_elements = document.getElementsByClassName('color-part')
        for (let index = 0; index < color_elements.length; index++)
            color_elements[index].style.backgroundColor = "";
        color_selected = {floor: "",wall_bottom: "",wall_right: ""};
        drawHouse();
    });

    function getColor() {
        if (!isMobile()) {
            let carousel_colors = document.getElementById('carousel-colors');
            for (let index = 0; index < carousel_colors.children.length; index++)
                if (carousel_colors.children[index].style['z-index'] == 0)
                    return color = carousel_colors.children[index].firstElementChild.lastElementChild.innerText;
        } else {
            console.log(document.getElementById('color').style.backgroundColor);
            return color = document.getElementById('color').style.backgroundColor;
        }
    }

    // Select color
    $('.color-select').on('click', function (e) {
        let element = e.target.parentElement.parentElement.lastElementChild.firstElementChild.firstElementChild.firstElementChild.firstElementChild;
        let color = getColor();
        
        if (isMobile()) {
            element.style.backgroundColor = color;
        }

        switch (element.id) {
            case "color-floor":
                color_selected.floor = color;
                break;
            case "color-wall_bottom":
                color_selected.wall_bottom = color;
                break;
            case "color-wall_right":
                color_selected.wall_right = color;
                break;
            default:
                break;
        }
        element.style.backgroundColor = color;
        drawHouse();
    });

    $('#canvas-house').on('contextmenu', function(e) {
        return false;
    });


    $('#canvas-house').on('mousedown', function(e) {
        let rect = canvas.getBoundingClientRect();
        let mouseY = (e.originalEvent.clientY - rect.top) * (canvas.height / rect.height);
        let mouseX = (e.originalEvent.clientX - rect.left) * (canvas.width / rect.width);
        
        console.log("X: "+mouseX+" Y:"+mouseY);
        console.log("X: "+rect.width+" Y:"+rect.height);

        canvas_shapes.forEach(shape => {
            shapeDefine(shape.nodes);
            if (ctx.isPointInPath(mouseX,mouseY)) {
                switch (shape.id) {
                    case "floor":
                        color_selected.floor = getColor();
                        break;
                    case "wall_bottom":
                        color_selected.wall_bottom = getColor();
                        break;
                    case "wall_right":
                        color_selected.wall_right = getColor();
                        break;
                    default:
                        break;
                }
                console.log('asd');
                console.log(color_selected);
                drawHouse();
            }
        });
    });

    $('#carousel-colors').carousel('next',Math.floor(Math.random() * 10));
});


$(document).ready(function () {
    $('.carousel').carousel();
    $('.dropdown-trigger').dropdown();
    $('.parallax').parallax();
});