<?php

include 'conn.php';

    if (isset($_POST['form_name'])) {

        try {
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $color = $_POST['form_colorm'].$_POST['form_colorf'];
            // prepare sql and bind parameters
            $stmt = $conn->prepare("INSERT INTO tilaus (nimi, osoite, pnro, ppaikka, nelio, pvm, maali)
            VALUES (:nimi, :osoite, :pnro, :ppaikka, :nelio, :pvm, :maali)");
            $stmt->bindParam(':nimi', $_POST['form_name']);
            $stmt->bindParam(':osoite', $_POST['form_name']);
            $stmt->bindParam(':pnro', $_POST['form_mail']);
            $stmt->bindParam(':ppaikka', $_POST['form_mailp']);
            $stmt->bindParam(':nelio', $_POST['form_area']);
            $stmt->bindParam(':pvm', $_POST['form_date']);
            $stmt->bindParam(':maali', $color);
            $stmt->execute();
            $stmt = null;
            $conn = null;
            
        } catch (PDOExeption $e) {
            echo $e;
        }


    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tulosta</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    

     <h1>Tilausvahvistus</h1>
     <?php
     if (isset($_POST['form_name'])) {
         echo '
        <b>Asiakas</b><br>
        '.$_POST['form_name'].'<br>'.
        $_POST['form_address'].'<br><br>
        <b>Toimittaja</b><br>
        Masan maalaus- ja tapetointi<br><br>
        <b>Toimituspäivä</b><br>
        <br>
        <table>
        <tr>
            <th>Määrä</th>
            <th>Kuvaus</th>
            <th>Yksikköhinta</th>
            <th>Yhteensä</th>
        </tr>
        <tr>
            <td>1</td>
            <td>Maali '.$_POST['form_colorm'].' '.$_POST['form_colorf'].'</td>
            <td id="amount-paint"></td>
            <td id="amount-paint2"></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Työtunnit</td>
            <td id="amount-hours"></td>
            <td id="amount-hours2"></td>
        </tr>
        <tr>
            <td colspan="2">Maksettava yhteensä</td>
            <td></td>
            <td id="amount-total"></td>
        </tr>
    </table>
         ';
     }
     ?>
     <br><b>Kiitos palvelujemme käyttämisestä</b><br>
    <h2>Masan maalaus- ja tapetointi</h2>
    Rautatieläisenkatu 12, 00520 Helsinki, GSM: +358 50 3331234, Email: masa@masanmjat.fi, www.masan.fi
</body>
</html>
