<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Tilaus</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="src/main.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="src/main.js"></script>
</head>
<body>
    
    <form action="/prompt.php" method="post">
        <table>
            <tr>
                <td>Nimi</td>
                <td>
                    <input required type="text" name="form_name">
                </td>
            </tr>
            <tr>
                <td>Katuosoite</td>
                <td>
                    <input required type="text" name="form_address">
                </td>
            </tr>
            <tr>
                <td>Postipaikka</td>
                <td>
                    <input required type="text" name="form_mailp">
                </td>
            </tr>
            <tr>
                <td>Postinumero</td>
                <td>
                    <input required type="number" name="form_mail">
                </td>
            </tr>
            <tr>
                <td>Alueen neliöt</td>
                <td>
                   <input required type="number" name="form_area" id="">
                </td>
            </tr>
            <tr>
                <td>Aikatoive</td>
                <td>
                    <input required id="form_date" type="date" name="form_date" id="">
                </td>
            </tr>
            <tr>
                <td>Maalin väri</td>
                <td>
                    <select required name="form_colorm" id="">
                        <option value="matta">matta</option>
                        <option value="kirkas">kirkas</option>
                    </select>
                    <select required name="form_colorf" id="">
                        <option value="musta">musta</option>
                        <option value="valkoinen">valkoinen</option>
                        <option value="vihreä">vihreä</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <input required type="submit" value="Lähetä">
                </td>
            </tr>
        </table>
    </form>

    <script>
    var dates = [<?php

        include 'conn.php';

        try {
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // prepare sql and bind parameters
            $stmt = $conn->prepare("SELECT * FROM tilaus");
            $stmt->execute();

            while ($row = $stmt->fetch()) {
                echo '"'.$row['pvm'].'",';
            }
            $stmt = null;
            $conn = null;
            
        }
        catch (PDOException $e) {}?>];
    
    var oc_dates = [];
    dates.forEach(date => {
        let s = date.split('.');
        oc_dates.push(s[2]+"-"+s[0]+"-"+s[0])
    });


    $('#form_date').on('change',function(e) {
        oc_dates.forEach(date => {
            if (e.target.value == date) {
                alert('Päivä on jo varattu. Valitse toinen.');
                e.target.value = "";
            }
        });
    });
    
    </script>

</body>
</html>