<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Varmista tilaus</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="src/main.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="src/main.js"></script>
</head>
<body>
    <table>
        <?php
            if (isset($_POST['form_address'])) {
                echo '
                <tr>
                    <td>Nimi</td>
                    <td id="form_name">
                        '.$_POST['form_name'].'
                    </td>
                </tr>
                <tr>
                    <td>Katuosoite</td>
                    <td id="form_address">
                    '.$_POST['form_address'].'
                    </td>
                </tr>
                <tr>
                    <td>Postipaikka</td>
                    <td id="form_mailp">
                    '.$_POST['form_mailp'].'
                    </td>
                </tr>
                <tr>
                    <td>Postinumero</td>
                    <td id="form_mail">
                    '.$_POST['form_mail'].'
                    </td>
                </tr>
                <tr>
                    <td>Alueen neliöt</td>
                    <td id="form_area">
                    '.$_POST['form_area'].'
                    </td>
                </tr>
                <tr>
                    <td>Aikatoive</td>
                    <td id="form_date">
                    '.$_POST['form_date'].'
                    </td>
                </tr>
                <tr>
                    <td>Maalin väri</td>
                    <td id="form_paint">Maali '.$_POST['form_colorm'].'/'.$_POST['form_colorf'].'</td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" value="Lähetä">
                    </td>
                </tr>
                ';
            }
        ?>
    </table>
    </form>
    
    <table>
        <tr>
            <th>Määrä</th>
            <th>Kuvaus</th>
            <th>Yksikköhinta</th>
            <th>Yhteensä</th>
        </tr>
        <tr>
            <td>1</td>
            <td><?php echo 'Maali '.$_POST['form_colorm'].'/'.$_POST['form_colorf'];?></td>
            <td id="amount-paint"></td>
            <td id="amount-paint2"></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Työtunnit</td>
            <td id="amount-hours"></td>
            <td id="amount-hours2"></td>
        </tr>
        <tr>
            <td colspan="2">Maksettava yhteensä</td>
            <td></td>
            <td id="amount-total"></td>
        </tr>

        <tr id="rc">
            <td><input type="button" id="button-order" value="Tilaa"></td>
        </tr>

    </table>
    


    <script>
        var area = Number(document.getElementById('form_area').innerText);
        var working_hours;
        var total_paint = Math.floor(Number(area) / 5);
        document.getElementById('amount-paint').innerText = total_paint;
        document.getElementById('amount-paint2').innerText =  total_paint;
        if (area < 10) {
            working_hours = 2;
        } else {
            working_hours = 6;
        }
        document.getElementById('amount-hours').innerText = working_hours;
        document.getElementById('amount-hours2').innerText =  working_hours;
        document.getElementById('amount-total').innerText =  working_hours + total_paint;

        let x = confirm('Tehdäänkö tilaus?');

        if (!x) {
            document.getElementById('rc').style.display = "none";
        }


        $('#button-order').click(function(){
            let x = confirm('Haluatko tulostaa tilausvahvistuksen?');

            var data = {
                form_name:document.getElementById('form_name').innerText,
                form_address:document.getElementById('form_address').innerText,
                form_mailp:document.getElementById('form_mailp').innerText,
                form_mail:document.getElementById('form_mail').innerText,
                form_area:document.getElementById('form_area').innerText,
                form_date:document.getElementById('form_date').innerText,
                form_colorm:<?php echo '"'.$_POST['form_colorm'].'"';?>,
                form_colorf:<?php echo '"'.$_POST['form_colorf'].'"';?>
            }

            $.post('print.php',data,function(data,res){
                    if (x) {
                        print(res);
                    }
                });
        });



    </script>


</body>
</html>